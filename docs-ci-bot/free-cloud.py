import argparse
import os
import yaml
from google.cloud import run_v2
from google.oauth2 import service_account
from google.iam.v1 import iam_policy_pb2
from google.iam.v1 import policy_pb2
from google.api_core.exceptions import NotFound, PermissionDenied

def load_specs(file):
    try:
        with open(file, 'r') as file:
            params = yaml.safe_load(file)
            specs = params["cloud_run"]
            return specs
    except FileNotFoundError:
        print(f"Error: File '{file}' not found.")
        return

def load_service_account_from_env():
    """Loads a service account from a JSON string stored in an environment variable."""
    try:
        service_account_info = os.environ['GCP_SERVICE_ACCOUNT']
        return service_account.Credentials.from_service_account_info(yaml.safe_load(service_account_info))
    except KeyError:
        print("Error: Environment variable 'GCP_SERVICE_ACCOUNT' not found.")
        exit(1)

def set_iam_policy(credentials, project_id, region, service_name):
    client = run_v2.ServicesClient(credentials=credentials)
    resource = f"projects/{project_id}/locations/{region}/services/{service_name}"

    # Check if the service exists
    try:
        service = client.get_service(name=resource)
    except NotFound:
        print(f"Service {service_name} not found.")
        return
    except PermissionDenied:
        print(f"Permission denied when accessing {service_name}.")
        return

    policy = policy_pb2.Policy(
        bindings=[
            policy_pb2.Binding(
                role="roles/run.invoker",
                members=["allUsers"]
            )
        ]
    )

    request = iam_policy_pb2.SetIamPolicyRequest(
        resource=resource,
        policy=policy
    )

    try:
        response = client.set_iam_policy(request=request)
        print(f"Unauthenticated access granted to service {service_name}")
    except PermissionDenied as e:
        print(f"Permission denied when setting IAM policy: {e}")

def create_cloud_run_service():
    credentials = load_service_account_from_env()
    client = run_v2.ServicesClient(credentials=credentials)
    specs = load_specs("requirements.yaml")

    parent = f"projects/{specs['project_id']}/locations/{specs['region']}"

    container = run_v2.Container(
        image=specs['container_image'],
    )

    template = run_v2.RevisionTemplate(
        containers=[container]
    )

    service = run_v2.Service(
        template=template
    )

    request = run_v2.CreateServiceRequest(
        parent=parent,
        service=service,
        service_id=specs['service_name']
    )

    operation = client.create_service(request=request)
    client.set_iam_policy
    print(f"Service {specs['service_name']} created: {operation.result()}")

    # TODO check params and see what operation returns
    set_iam_policy(credentials, specs['project_id'], specs['region'], specs['service_name'])


def list_cloud_run_services():
    credentials = load_service_account_from_env()
    client = run_v2.ServicesClient(credentials=credentials)
    specs = load_specs("requirements.yaml")

    parent = f"projects/{specs['project_id']}/locations/{specs['region']}"

    request = run_v2.ListServicesRequest(
        parent=parent
    )

    response = client.list_services(request=request)

    print("Cloud Run services:")
    for service in response.services:
        print(f"- {service.name}")

def delete_cloud_run_service():
    credentials = load_service_account_from_env()
    client = run_v2.ServicesClient(credentials=credentials)
    specs = load_specs("requirements.yaml")

    name = f"projects/{specs['project_id']}/locations/{specs['region']}/services/{specs['service_name']}"

    request = run_v2.DeleteServiceRequest(
        name=name
    )

    operation = client.delete_service(request=request)
    print(f"Service {specs['service_name']} deleted: {operation.result()}")

def main():
    parser = argparse.ArgumentParser(description="GCP Cloud Run CLI")
    parser.add_argument("action", choices=["list", "create", "delete"], help="Action to perform")
    args = parser.parse_args()

    if args.action == "create":
        create_cloud_run_service()
    elif args.action == "list":
        list_cloud_run_services()
    elif args.action == "delete":
        delete_cloud_run_service()

if __name__ == "__main__":
    main()