# Kubernetes Today Blog

## Overview

Welcome to the Kubernetes Today Blog repository. This repository houses the source code and content for our Kubernetes tutorials and associated technical articles, which are available at [kubernetestoday.tech](https://kubernetestoday.tech). The blog content, stored under the `posts` directory as markdown files, is transformed into static HTML content through a custom Python script. Any changes pushed to the main branch are automatically deployed to GitLab Pages.

We invite those interested in sharing their own content to contribute to our community! For more details, please refer to the [Contributing](#contributing) section below.

## Features

- **Markdown-based Content:** Easy to write and manage tutorials.
- **Static Site Generation:** Custom Python script to generate HTML using Jinja2 templates.
- **Continuous Deployment:** Automated deployment pipeline to GitLab Pages.
- **Merge Request Previews:** Bot-enabled previews for every merge request or update, ensuring content accuracy and quality before going live.

## Getting Started

### Prerequisites

- Git
- Python 3
- Any text editor (VSCode, Atom, etc.)

### Setting Up Your Local Environment

1. **Clone the Repository**

    ```bash
    git clone https://gitlab.com/kubernetes-today/kubernetes-today-blog.git
    cd kubernetes-tech
    ```

2. **Install Dependencies**

    The Python script may require specific libraries. Install them using:

    ```bash
    pip install -r requirements.txt
    ```

3. **Run the Script**

    Generate the static HTML content by running:

    ```bash
    python generate.py
    ```

### Directory Structure

- `posts/`: Markdown files for each blog post.
- `templates/`: Jinja2 templates with html and css for the website.
- `generate.py`: Python script that iterates the markdown files under posts and generates static html content.
- `.gitlab-ci.yml`: CICD pipeline that will validate site and deploy it to gitlab pages.

## Contributing

Want to publish your own article in our blog? We welcome contributions to the Kubernetes Tech Blog! Here’s how you can help:

1. **Fork the Repository:** Start by forking the repository and cloning it locally.
2. **Create a New Branch:** Work on new features or fixes in a separate branch:

    ```bash
    git checkout -b your-feature-branch
    ```

3. **Add Your Own Post:** Feel free to add new posts by creating a markdown file in the `posts` directory. Make sure to include the date, author, and tags for each post to ensure proper organization and accessibility. You can copy this template [example post](posts/article.contributing.example.md).
4. **Test Locally:** Generate the static site to test your changes.
5. **Submit a Merge Request:** Push your changes and submit a merge request. Make sure your merge request description is clear.

## Deployment

Changes to the main branch trigger an automatic deployment pipeline that builds the site and pushes it to GitLab Pages. Ensure your changes pass all necessary checks before merging.