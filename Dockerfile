# Use the official Red Hat UBI9 minimal image as the base image
FROM registry.access.redhat.com/ubi9-minimal:9.1

# Set the user to root for installing packages and making system changes
USER root

# Set environment variables
ENV TZ=UTC \
    HOME=/app \
    PATH=$HOME/.local/bin:$PATH

# Set the working directory to the home directory
WORKDIR $HOME

# Install necessary packages, set timezone, and clean up in a single RUN command to minimize layers
RUN microdnf install --nodocs -y bash git shadow-utils python39 python3-pip && \
    microdnf clean all && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone

COPY requirements.* $HOME/

RUN pip3 install --no-cache-dir -r $HOME/requirements.txt && \
    chown -R 1001:1001 $HOME

COPY entrypoint.sh $HOME/

# Change to a non-root user for running the application
USER 1001

ENTRYPOINT ["bash", "entrypoint.sh"]
